from rest_framework import permissions
from rest_framework.permissions import SAFE_METHODS

__all__ = [
    'IsOwnerProfileOrAdmin',
]


class IsOwnerProfileOrAdmin(permissions.BasePermission):
    """
    Check if user is owner of profile or he is Admin for unsafe methods
    """
    def has_object_permission(self, request, view, obj):
        if request.method not in SAFE_METHODS:
            if request.user == obj.user or request.user.is_superuser:
                return True
            return False

        return True
