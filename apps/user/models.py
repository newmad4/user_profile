from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.utils.translation import ugettext as _

from versatileimagefield.fields import VersatileImageField

from user.managers import CustomUserManager

__all__ = [
    "User",
    "UserProfile",
]


class User(AbstractBaseUser, PermissionsMixin):
    """
    User model in our service. Contain minimum information about user - only service information with email
    """
    email = models.EmailField(unique=True, help_text=_("User email. Unique user identification"))
    date_joined = models.DateTimeField(auto_now_add=True, help_text=_("When user connect to service"))
    is_staff = models.BooleanField(
        verbose_name=_("staff status"),
        default=False,
        help_text=_("Designates whether the user can log into this admin site."),
    )
    is_active = models.BooleanField(
        default=True, help_text=_(
            "Designates whether this user should be treated as active. Unselect this instead of deleting accounts"
        ),
    )
    objects = CustomUserManager()
    USERNAME_FIELD = "email"

    def __str__(self):
        return self.email


class UserProfile(models.Model):
    """
    User profile - contains detail User information
    """
    user = models.OneToOneField(User, related_name="profile", on_delete=models.CASCADE, help_text=_("Owner of profile"))
    dt_created = models.DateTimeField(auto_now_add=True, help_text=_("When profile was created"))
    dt_updated = models.DateTimeField(auto_now=True, help_text=_("When profile last time was updated"))
    username = models.CharField(max_length=30, help_text=_("User name for interaction with other users"))
    is_email_confirmed = models.BooleanField(default=False, help_text=_("Did user confirm his email?"))
    avatar = VersatileImageField(
        upload_to="images/",
        default="images/avatar-default-icon.png",
        help_text=_("User avatar picture")
    )
    birth_date = models.DateField(null=True, blank=True, help_text=_("User birth date"))
