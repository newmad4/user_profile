from rest_framework import serializers

from user.models import UserProfile


__all__ = [
    "UserProfileSerializer",
    "UserProfileListSerializer"
]


class UserProfileSerializer(serializers.ModelSerializer):
    """
    Serializer for single user profile data
    """
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    email = serializers.EmailField(source="user.email", read_only=True)
    date_joined = serializers.DateTimeField(source="user.date_joined", read_only=True)

    class Meta:
        model = UserProfile
        fields = (
            "id", "email", "username", "avatar", "date_joined", "username", "is_email_confirmed", "birth_date", "user"
        )
        read_only_fields = ("id", "is_email_confirmed", "email", "date_joined")

    def validate_user(self, value):
        """
        Check if User profile already exists for current user
        """
        if UserProfile.objects.filter(user=value).exists():
            raise serializers.ValidationError("UserProfile already exists! Now you could only update or delete it.")
        return value


class UserProfileListSerializer(serializers.ModelSerializer):
    """
    Serializer for list users profiles
    """
    class Meta:
        model = UserProfile
        fields = ("id", "username", "user")
