from rest_framework.routers import DefaultRouter
from django.urls import path, include

from .views import UserProfileViewSet


router = DefaultRouter()
router.register('', UserProfileViewSet)

app_name = 'user-profile'
urlpatterns = [
    path('', include(router.urls)),
]
