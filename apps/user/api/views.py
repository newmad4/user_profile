from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticatedOrReadOnly

from user.api.serializers import UserProfileSerializer, UserProfileListSerializer
from user.models import UserProfile
from user.permissions import IsOwnerProfileOrAdmin


__all__ = [
    "UserProfileViewSet"
]


class UserProfileViewSet(viewsets.ModelViewSet):
    """
    API view set which get CRUD operations with UserProfile data
    """
    permission_classes = (IsAuthenticatedOrReadOnly, IsOwnerProfileOrAdmin)
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer

    def get_serializer_class(self):
        if self.action == 'list':
            return UserProfileListSerializer

        return self.serializer_class



