# user_profile

Live site could be found [here](https://test-work-user-profile.herokuapp.com/)

Assumptions:
1. I prefer to create model UserProfile - model which would store any custom User data, such as: avatar, birth_date, gender, etc.
2. I understand "Administrator of system" - native Django admin. Of course - if wee need separate role, wee could add add field like "role" - which would be choice field(ADMIN, MANAGER, CLIENT,...).
3. I redefine new User model and keep only service attributes.
4. For authorization i used JWT.
5. Main User field is "email" field.
